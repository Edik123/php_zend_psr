<?php

namespace Framework\Template;

use Twig\TwigFunction;
use Twig\Extension\AbstractExtension;

/**
 * Twig extension for the Laravel Mix component
 */
class MixExtension extends AbstractExtension
{
    protected $ressourcesAssets;
    protected $publicDir;
    protected $manifestName;
    protected $manifest;

    public function __construct($publicDir = "public", $manifestName = 'mix-manifest.json')
    {
        $this->publicDir = rtrim($publicDir, '/') ;
        $this->manifestName = $manifestName;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('mix', [$this, 'getVersionedFilePath']),
        ];
    }

    /**
     * Gets the public url/path to a versioned Mix file.
     *
     * @param string $file
     *
     * @return string
     *
     * @throws \InvalidArgumentException
     */
    public function getVersionedFilePath($file)
    {
        $manifest = $this->getManifest();

        if (!isset($manifest[$file])) {
            throw new \InvalidArgumentException("File {$file} not defined in asset manifest.");
        }
        //Only return the file path relative to the public folder (e.g css/style.css) and not (/public/css/style)
        return $manifest[$file];
    }

    /**
     * Returns the manifest file content as array.
     *
     * @return array
     */
    protected function getManifest()
    {
        if (null === $this->manifest) {
            $manifestPath = $this->publicDir.'/'.$this->manifestName;
            $this->manifest = json_decode(file_get_contents($manifestPath), true);
        }

        return $this->manifest;
    }
}