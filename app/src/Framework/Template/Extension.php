<?php

namespace Framework\Template;

abstract class Extension
{
    public function getFunctions(): array
    {
        return [];
    }
}
