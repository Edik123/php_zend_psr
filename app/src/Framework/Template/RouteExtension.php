<?php

namespace Framework\Template;

use Twig\TwigFunction;
use Twig\Extension\AbstractExtension;
use Framework\Http\Router\RouterInterface;

class RouteExtension extends AbstractExtension
{
    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('path', [$this, 'generatePath']),
        ];
    }

    public function generatePath($name, array $params = []): string
    {
        return $this->router->generate($name, $params);
    }
}
