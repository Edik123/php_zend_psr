<?php

namespace Framework;

use Psr\Container\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;

class PDOFactory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var EntityManagerInterface $em */
        $em = $container->get(EntityManagerInterface::class);
        
        return $em->getConnection()->getWrappedConnection();
    }
}