<?php

namespace App\Http\Middleware\ErrorHandler;

use Zend\Diactoros\Response;
use Zend\Stratigility\Utils;
use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Psr\Http\Message\ServerRequestInterface;

class HtmlErrorResponseGenerator implements ErrorResponseGeneratorInterface
{
    public function generate(\Throwable $e, ServerRequestInterface $request): ResponseInterface
    {
        return new HtmlResponse('Server error', Utils::getStatusCode($e, new Response()));
    }
}
