<?php

namespace App\Http\Middleware\ErrorHandler;

use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response;
use Zend\Stratigility\Utils;

class JsonErrorResponseGenerator implements ErrorResponseGeneratorInterface
{
    public function generate(\Throwable $e, ServerRequestInterface $request): ResponseInterface
    {
        return new JsonResponse([
            'error' => 'Server error',
            'code' => $e->getCode(),
            'message' => $e->getMessage(),
        ], Utils::getStatusCode($e, new Response()));
    }
}
