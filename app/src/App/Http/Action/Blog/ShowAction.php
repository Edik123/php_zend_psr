<?php

namespace App\Http\Action\Blog;

use App\Repository\PostRepository;
use Framework\Template\TwigRenderer;
use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\EmptyResponse;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ShowAction implements RequestHandlerInterface
{
    private $template;
    private $postRepository;

    public function __construct(PostRepository $postRepository, TwigRenderer $template)
    {
        $this->template = $template;
        $this->postRepository = $postRepository;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        if (!$post = $this->postRepository->find($request->getAttribute('id'))) {
            return new EmptyResponse(404);
        }

        return new HtmlResponse($this->template->render('blog/show', [
            'post' => $post
        ]));
    }
}
