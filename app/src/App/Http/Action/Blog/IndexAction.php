<?php

namespace App\Http\Action\Blog;

use App\DTO\Pagination;
use App\Repository\PostRepository;
use Framework\Template\TwigRenderer;
use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class IndexAction implements RequestHandlerInterface
{
    private const PER_PAGE = 5;
    
    private $template;
    private $postRepository;

    public function __construct(PostRepository $postRepository, TwigRenderer $template)
    {
        $this->template = $template;
        $this->postRepository = $postRepository;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $pages = new Pagination(
            $this->postRepository->countAll(),
            $request->getAttribute('page') ?: 1,
            self::PER_PAGE
        );

        $posts = $this->postRepository->getAll(
            $pages->getOffset(),
            $pages->getLimit()
        );

        return new HtmlResponse($this->template->render('blog/index', [
            'posts' => $posts,
            'pages' => $pages
        ]));
    }
}
