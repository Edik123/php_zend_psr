<?php

namespace App\Repository;

use App\Entity\Post;
use Doctrine\ORM\EntityRepository;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\ORM\EntityManagerInterface;

class PostRepository
{
    /**
    * @var EntityRepository $repository
    */
    private $repository;

    /**
     * @var Connection $pdo;
     */
    private $pdo;

    public function __construct(Connection $pdo)
    {
        //$this->repository = $em->getRepository(Post::class);
        $this->pdo = $pdo;
    }

    /**
     * @return Post[]
     */
    public function getAll(int $offset, int $limit): array
    {
        /*return $this->repository
            ->createQueryBuilder('p')
            ->select('p')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->orderBy('p.createDate', 'DESC')
            ->getQuery()
            ->getResult();*/
        
        $stmt = $this->pdo->prepare('
            SELECT
                p.*,
                (SELECT COUNT(*) FROM comments c WHERE c.post_id = p.id) comments_count
            FROM posts p ORDER BY p.create_date DESC LIMIT :limit OFFSET :offset
        ');

        $stmt->bindValue(':limit', $limit, \PDO::PARAM_INT);
        $stmt->bindValue(':offset', $offset, \PDO::PARAM_INT);

        $stmt->execute();

        return array_map([$this, 'hydratePostList'], $stmt->fetchAll());
    }

    public function countAll(): int
    {
        /*return $this->repository
            ->createQueryBuilder('p')
            ->select('COUNT(p)')
            ->getQuery()
            ->getSingleScalarResult();*/

            return $this->pdo->query('SELECT COUNT(id) FROM posts')->fetchColumn();
    }

    /*public function find($id): ?Post
    {
        return $this->repository->find($id);
    }*/

    public function find(int $id): ?array
    {
        $stmt = $this->pdo->prepare('SELECT p.* FROM posts p WHERE id = :id');
        $stmt->bindValue(':id', $id, \PDO::PARAM_INT);
        $stmt->execute();

        if (!$post = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            return null;
        }

        $stmt = $this->pdo->prepare('SELECT * FROM comments WHERE post_id = :post_id ORDER BY id ASC');
        $stmt->bindValue(':post_id', (int)$post['id'], \PDO::PARAM_INT);
        $stmt->execute();
        $comments = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $this->hydratePostDetail($post, $comments);
    }

    private function hydratePostList(array $row): array
    {
        return [
            'id' => (int)$row['id'],
            'date' => new \DateTimeImmutable($row['create_date']),
            'title' => $row['title'],
            'preview' => $row['content_short'],
            'commentsCount' => $row['comments_count'],
        ];
    }

    private function hydratePostDetail(array $row, array $comments): array
    {
        return [
            'id' => (int)$row['id'],
            'date' => new \DateTimeImmutable($row['create_date']),
            'title' => $row['title'],
            'content' => $row['content_full'],
            'meta' => [
                'title' => $row['meta_title'],
                'description' => $row['meta_description'],
            ],
            'comments' => array_map([$this, 'hydrateComment'], $comments),
        ];
    }

    private function hydrateComment(array $row): array
    {
        return [
            'id' => (int)$row['id'],
            'date' => new \DateTimeImmutable($row['date']),
            'author' => $row['author'],
            'text' => $row['text'],
        ];
    }
}
