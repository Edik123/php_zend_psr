<?php

namespace Tests\App\Http\Action;

use PHPUnit\Framework\TestCase;
use App\Http\Action\HelloAction;
use Zend\Diactoros\ServerRequest;
use Framework\Template\TemplateRenderer;

class HelloActionTest extends TestCase
{
    private $renderer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->renderer = new TemplateRenderer('templates');
    }
    
    public function testGuest()
    {
        $action = new HelloAction($this->renderer);
        $response = $action->handle(new ServerRequest());

        self::assertEquals(200, $response->getStatusCode());
        self::assertStringContainsString('Hello, Guest!', $response->getBody()->getContents());
    }

    public function testJohn()
    {
        $action = new HelloAction($this->renderer);

        $request = (new ServerRequest())
            ->withQueryParams(['name' => 'John']);

        $response = $action->handle($request);

        self::assertStringContainsString('Hello, John!', $response->getBody()->getContents());
    }
}