<?php

namespace Tests\App\Http\Handler\Blog;

use PHPUnit\Framework\TestCase;
use Zend\Diactoros\ServerRequest;
use App\Http\Action\Blog\IndexAction;

class IndexActionTest extends TestCase
{
    public function testSuccess()
    {
        $action = new IndexAction();
        $response = $action->handle(new ServerRequest());

        self::assertEquals(200, $response->getStatusCode());
        self::assertJsonStringEqualsJsonString(
            json_encode([
                ['id' => 2, 'title' => 'The Second Post'],
                ['id' => 1, 'title' => 'The First Post'],
            ]),
            $response->getBody()->getContents()
        );
    }
}
