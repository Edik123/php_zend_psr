<?php

use Monolog\Logger;
use App\Http\Action;
use App\Service\FileManager;
use Zend\Diactoros\Response;
use App\Command\FixtureCommand;
use Framework\Http\Application;
use Framework\Http\Router\Router;
use App\Command\CacheClearCommand;
use App\Repository\PostRepository;
use Framework\Http\HandlerResolver;
use Framework\Template\MixExtension;
use Framework\Template\TwigRenderer;
use Psr\Container\ContainerInterface;
use Framework\Template\RouteExtension;
use App\Http\Middleware\NotFoundHandler;
use Doctrine\ORM\EntityManagerInterface;
use Framework\Template\TemplateRenderer;
use App\Http\Middleware\ProfilerMiddleware;
use App\Http\Middleware\BasicAuthMiddleware;
use Framework\Http\Router\AuraRouterAdapter;
use App\Http\Middleware\CredentialsMiddleware;
use Framework\Http\Middleware\RouteMiddleware;
use App\Http\Middleware\EmptyResponseMiddleware;
use App\Http\Middleware\ResponseLoggerMiddleware;
use Framework\Http\Middleware\DispatchMiddleware;
use Framework\Http\Middleware\BodyParamsMiddleware;
use App\Http\Middleware\ErrorHandler\LogErrorListener;
use Doctrine\DBAL\Migrations\Provider\OrmSchemaProvider;
use App\Http\Middleware\ErrorHandler\ErrorHandlerMiddleware;
use Doctrine\DBAL\Migrations\Tools\Console\Command\DiffCommand;
use App\Http\Middleware\ErrorHandler\HtmlErrorResponseGenerator;
use App\Http\Middleware\ErrorHandler\WhoopsErrorResponseGenerator;
use App\Http\Middleware\ErrorHandler\ErrorResponseGeneratorInterface;

return [
    'dependencies' => [
        /*'abstract_factories' => [
            ReflectionBasedAbstractFactory::class,
        ],*/
        'invokables' => [
            Doctrine\DBAL\Migrations\Tools\Console\Command\ExecuteCommand::class,
            Doctrine\DBAL\Migrations\Tools\Console\Command\GenerateCommand::class,
            Doctrine\DBAL\Migrations\Tools\Console\Command\LatestCommand::class,
            Doctrine\DBAL\Migrations\Tools\Console\Command\MigrateCommand::class,
            Doctrine\DBAL\Migrations\Tools\Console\Command\DiffCommand::class,
            Doctrine\DBAL\Migrations\Tools\Console\Command\UpToDateCommand::class,
            Doctrine\DBAL\Migrations\Tools\Console\Command\StatusCommand::class,
            Doctrine\DBAL\Migrations\Tools\Console\Command\VersionCommand::class,
            CredentialsMiddleware::class,
            BodyParamsMiddleware::class,
            ProfilerMiddleware::class,
            FileManager::class
        ],
        'factories' => [
            Application::class => function (ContainerInterface $container) {
                return new Application(
                    $container->get(HandlerResolver::class),
                    $container->get(Router::class),
                    new NotFoundHandler($container->get(TemplateRenderer::class))
                );
            },
            Action\HelloAction::class => function (ContainerInterface $container) {
                return new Action\HelloAction($container->get(TemplateRenderer::class));
            },
            Action\AboutAction::class => function (ContainerInterface $container) {
                return new Action\AboutAction($container->get(TemplateRenderer::class));
            },
            Action\CabinetAction::class => function (ContainerInterface $container) {
                return new Action\CabinetAction($container->get(TemplateRenderer::class));
            },
            Action\Blog\IndexAction::class => function (ContainerInterface $container) {
                return new Action\Blog\IndexAction(
                    $container->get(PostRepository::class),
                    $container->get(TemplateRenderer::class)
                );
            },
            Action\Blog\ShowAction::class => function (ContainerInterface $container) {
                return new Action\Blog\ShowAction(
                    $container->get(PostRepository::class),
                    $container->get(TemplateRenderer::class)
                );
            },
            EntityManagerInterface::class => function(ContainerInterface $container) {
                return (new ContainerInteropDoctrine\EntityManagerFactory())($container);
            },
            PostRepository::class => function (ContainerInterface $container) {
                return new PostRepository(
                    //$container->get(EntityManagerInterface::class)
                    $container->get(PDO::class)
                );
            },
            Router::class => function () {
                return new AuraRouterAdapter(new Aura\Router\RouterContainer());
            },
            HandlerResolver::class => function (ContainerInterface $container) {
                return new HandlerResolver($container, new Zend\Diactoros\Response());
            },
            BasicAuthMiddleware::class => function (ContainerInterface $container) {
                return new BasicAuthMiddleware($container->get('config')['users'], new Response());
            },
            DispatchMiddleware::class => function (ContainerInterface $container) {
                return new DispatchMiddleware($container->get(HandlerResolver::class));
            },
            RouteMiddleware::class => function (ContainerInterface $container) {
                return new RouteMiddleware($container->get(Router::class));
            },
            EmptyResponseMiddleware::class => function (ContainerInterface $container) {
                return new EmptyResponseMiddleware($container->get(TemplateRenderer::class));
            },
            RouteExtension::class => function (ContainerInterface $container) {
                return new RouteExtension($container->get(Router::class));
            },
            MixExtension::class => function (ContainerInterface $container) {
                $config = $container->get('config')['mix'];

                return new MixExtension(
                    $config['root'],
                    $config['manifest']
                );
            },
            CacheClearCommand::class => function (ContainerInterface $container) {
                return new CacheClearCommand(
                    $container->get(FileManager::class),
                    $container->get('config')['cachePaths']
                );
            },
            FixtureCommand::class => function (ContainerInterface $container) {
                return new FixtureCommand(
                    $container->get(EntityManagerInterface::class),
                    $container->get('config')['fixtures']
                );
            },
            DiffCommand::class => function (ContainerInterface $container) {
                return new DiffCommand(
                    new OrmSchemaProvider(
                        $container->get(EntityManagerInterface::class)
                    )
                );
            },
            PDO::class => function (ContainerInterface $container) {
                return (new Framework\PDOFactory())($container);
            },
            TemplateRenderer::class => function (ContainerInterface $container) {
                return new TwigRenderer($container->get(Twig\Environment::class), '.html.twig');
            },
            Twig\Environment::class => function(ContainerInterface $container)
            {
                $templateDir = 'templates';
                $cacheDir = 'var/cache/twig';
                $debug = $container->get('config')['debug'];

                $loader = new Twig\Loader\FilesystemLoader();
                $loader->addPath($templateDir);

                $environment = new Twig\Environment($loader, [
                    'cache' => $debug ? false : $cacheDir,
                    'debug' => $debug,
                    'strict_variables' => $debug,
                    'auto_reload' => $debug,
                ]);

                if ($debug) {
                    $environment->addExtension(new Twig\Extension\DebugExtension());
                }

                $environment->addExtension($container->get(RouteExtension::class));
                $environment->addExtension($container->get(MixExtension::class));

                return $environment;
            },
            ResponseLoggerMiddleware::class => function (ContainerInterface $container) {
                return new ResponseLoggerMiddleware(
                    $container->get(Psr\Log\LoggerInterface::class)
                );
            },
            LogErrorListener::class => function (ContainerInterface $container) {
                return new LogErrorListener(
                    $container->get(Psr\Log\LoggerInterface::class)
                );
            },
            ErrorHandlerMiddleware::class => function (ContainerInterface $container) {
                $middleware =  new ErrorHandlerMiddleware(
                    $container->get(ErrorResponseGeneratorInterface::class)
                );
                $middleware->addListener($container->get(LogErrorListener::class));
                return $middleware;
            },
            ErrorResponseGeneratorInterface::class => function (ContainerInterface $container) {
                if ($container->get('config')['debug']) {
                    return new WhoopsErrorResponseGenerator(
                        $container->get(Whoops\RunInterface::class),
                        new Zend\Diactoros\Response()
                    );
                    //return new JsonErrorResponseGenerator();
                }
                return new HtmlErrorResponseGenerator();
            },
            Whoops\RunInterface::class => function () {
                $whoops = new Whoops\Run();
                $whoops->allowQuit(false);
                $whoops->writeToOutput(false);
                $whoops->pushHandler(new Whoops\Handler\PrettyPageHandler());
                $whoops->register();
                return $whoops;
            },
            Psr\Log\LoggerInterface::class => function (ContainerInterface $container)
            {
                $logger = new Logger('App');
                $logger->pushHandler(new Monolog\Handler\StreamHandler(
                    dirname(__DIR__).'/var/log/application.log',
                    $container->get('config')['debug'] ? Logger::DEBUG : Logger::WARNING
                ));
                return $logger;
            },
        ]
    ]
];