<?php

use Zend\ConfigAggregator\PhpFileProvider;
use Zend\ConfigAggregator\ConfigAggregator;

$aggregator = new ConfigAggregator([
    new PhpFileProvider(__DIR__.'/{services,parameters,doctrine}.php'),
]);

return $aggregator->getMergedConfig();


/*return array_merge_recursive(
    require __DIR__ . '/services.php',
    require __DIR__ . '/parameters.php'
);*/

/*$configs = array_map(
    function ($file) {
        return require $file;
    },
    glob(__DIR__ .'/{services,parameters}.php', GLOB_BRACE)
);

return array_merge_recursive(...$configs);*/