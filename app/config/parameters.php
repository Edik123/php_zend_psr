<?php

use App\Command;
use Zend\ConfigAggregator\ConfigAggregator;

return [
    ConfigAggregator::ENABLE_CACHE => false,
    'debug' => true,
    'users' => [
        'admin' => 'password',
    ],
    'cachePaths' => [
        'doctrine' => 'var/cache/doctrine',
        'twig' => 'var/cache/twig',
        'db' => 'var/cache/db',
    ],
    'mix' => [
        'root' => 'public/build',
        'manifest' => 'mix-manifest.json',
    ],
    'fixtures' => 'db/fixtures',
    'commands' => [
        Command\FixtureCommand::class,
        Command\CacheClearCommand::class,
        Doctrine\DBAL\Migrations\Tools\Console\Command\ExecuteCommand::class,
        Doctrine\DBAL\Migrations\Tools\Console\Command\GenerateCommand::class,
        Doctrine\DBAL\Migrations\Tools\Console\Command\LatestCommand::class,
        Doctrine\DBAL\Migrations\Tools\Console\Command\MigrateCommand::class,
        Doctrine\DBAL\Migrations\Tools\Console\Command\DiffCommand::class,
        Doctrine\DBAL\Migrations\Tools\Console\Command\UpToDateCommand::class,
        Doctrine\DBAL\Migrations\Tools\Console\Command\StatusCommand::class,
        Doctrine\DBAL\Migrations\Tools\Console\Command\VersionCommand::class,
    ]
];