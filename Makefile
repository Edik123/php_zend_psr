up: docker-up
down: docker-down
restart: docker-down docker-up
init: docker-down-clear docker-pull docker-build docker-up app-init

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-down-clear:
	docker-compose down -v --remove-orphans

docker-pull:
	docker-compose pull

docker-build:
	docker-compose build

cli:
	docker-compose run --rm php-cli php bin/console

app-init: composer-install migrations fixtures

migrations:
	docker-compose run --rm php-cli php bin/app.php migrations:migrate --no-interaction

fixtures:
	docker-compose run --rm php-cli php bin/app.php fixtures:load --no-interaction

composer-install:
	docker-compose run --rm php-cli composer install

assets-install:
	docker-compose run --rm node yarn install
	docker-compose run --rm node npm rebuild node-sass
	docker-compose run --rm node npm run dev

test:
	docker-compose run --rm php-cli php vendor/bin/phpunit

ready:
	docker run --rm -v ${PWD}/app:/app --workdir=/app alpine touch .ready
